﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RESTAPIPresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Net.Http;
namespace RESTAPIPresentation.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : Controller
    {

        [HttpGet, Authorize]
        public IEnumerable<Student> Get()
        {
            var currentUser = HttpContext.User;
            return StudentMock.studentList;
        }
        [HttpGet, Authorize, Route("{studentnr}")]
        public IActionResult GetStudent([FromRoute]int studentnr)
        {
            var currentUser = HttpContext.User;
            Student stnt = StudentMock.studentList.Where((student) => student.nr == studentnr).FirstOrDefault();
            if (stnt != null)
            {
                return new OkObjectResult(stnt);
            }
            return NotFound();
        }
        [HttpPost, Authorize]
        public IEnumerable<Student> Post([FromBody]Student newStudent)
        {
            var currentUser = HttpContext.User;
            StudentMock.studentList.Add(newStudent);
            return StudentMock.studentList;
        }
        [HttpDelete, Authorize, Route("{studentnr}")]
        public IEnumerable<Student> Delete([FromRoute]int studentnr)
        {
            var currentUser = HttpContext.User;
            StudentMock.studentList.Remove(StudentMock.studentList.Where((student) => student.nr == studentnr).FirstOrDefault());
            return StudentMock.studentList;
        }



    }
}
