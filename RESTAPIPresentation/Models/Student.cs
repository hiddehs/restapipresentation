﻿using System;
using System.Collections.Generic;

namespace RESTAPIPresentation.Models
{
    public class Student
    {
        public int nr;
        public string name;

        public Student(int nr, string name)
        {
            this.nr = nr;
            this.name = name;
        }
    }
    public static class StudentMock
    {
        public static List<Student> studentList = new List<Student>() {
            new Student(1123007,"Tom"),
            new Student(1116146,"Tim"),
            new Student(1070409,"Quinten"),
            new Student(1123499,"Peter"),
            new Student(1118423,"Tim"),
            new Student(1118021,"Tjeerd"),
            new Student(1112986,"Jesse"),
            new Student(1114604,"Christiaan"),
            new Student(1113062,"Stefan"),
            new Student(1118729,"Gert Jan"),
            new Student(1117595,"Kevin"),
            new Student(1121445,"Thom"),
            new Student(1114434,"Jeffrey"),
            new Student(1116207,"Jeroen"),
            new Student(1116265,"Martijn"),
            new Student(1117534,"Erik"),
            new Student(1122813,"Hotse"),
            new Student(1095019,"Tycho"),
            new Student(1076879,"David"),
            new Student(1114639,"Sven"),
            new Student(1117786,"Thomas"),
            new Student(1103967,"Hidde"),
            new Student(1119636,"Nick"),
            new Student(1114202,"Olaf"),
            new Student(1117226,"Jeroen"),
            new Student(1113104,"Orhan"),
            new Student(1113099,"Pieter"),
            new Student(1113784,"Emil"),
            new Student(1118230,"Steffan"),
            new Student(1120652,"Rowan"),
            };
    }
}
